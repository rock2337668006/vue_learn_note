一、目前有研讀，並使用以下一些Vue的方法，簡單寫一些HTML文件(網頁)。
    1.Vue實例化、資料掛載、模版。(能在網頁印出一些訊息)
    2.Vue的數據、事件、方法。(透過點擊特定區塊，令網頁能有簡單反應)
    3.Vue的屬性綁定和雙向綁定。(依據輸入框的內容，動態改變其他區塊的內容)
    4.Vue的計算屬性、監聽器、v-if、v-show、v-for的指令等。

參考(研讀)網址：https://medium.com/@rorast.power.game/vue-js%E7%B3%BB%E5%88%97%E4%B8%80-%E5%85%A5%E9%96%80%E7%AF%87-9d131c9f3d39


二、大概了解如何使用Spring Boot與Vue.js來開發一個網頁專案，以及前後端分離的相關知識。

參考(研讀)網址：
    1.https://www.baeldung.com/spring-boot-vue-js
    2.https://ithelp.ithome.com.tw/articles/10243500



目前暫時寫到這邊，若有錯誤地方，歡迎大家指教！

